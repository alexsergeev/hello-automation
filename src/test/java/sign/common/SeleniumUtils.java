package sign.common;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.testng.Assert.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by alexsergeev on 4/26/16.
 */
public class SeleniumUtils {

    private static final long DEFAULT_SLEEP_TIMEOUT = 20;

    public static void assertElementExists(WebDriver driver, By by){
        assertNotNull(driver.findElement(by), "not found: " + by);
    }

    public static WebElement findElement(WebDriver driver, By by){
        return driver.findElement(by);
    }

    public static List<WebElement> findElements(WebDriver driver, By by){
        return driver.findElements(by);
    }

    public static void click(WebDriver driver, By by){
        findElement(driver, by).click();
    }

    public static WebElement clickUsingJavascript(WebDriver driver, By by){
        String script = "arguments[0].click();";
        WebElement button = waitForElementToBeClickable(driver, by);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        Object result = null;
        try {
            result = executor.executeScript(script, button);
        } catch(StaleElementReferenceException ex){
            button = waitForElementToBeClickable(driver, by);
            result = executor.executeScript(script, button);
        }
        return button;
    }

    public static void setText(WebDriver driver, By by, String text){
        WebElement element = findElement(driver, by);
        element.clear();
        element.sendKeys(text);
        verifyThatTextIsPresentTextField(driver, by, text);
    }

    public static String getText(WebDriver driver, By by){
        return findElement(driver,by).getText();
    }

    public static void verifyThatTextIsPresentTextField(WebDriver driver, By by, String expectedText){
        assertEquals(findElement(driver, by).getAttribute("value"), expectedText);
    }

    public static void verifyThatTextIsPresent(WebDriver driver, By by, String expectedText){
        assertEquals(getText(driver, by), expectedText);
    }

    public static WebElement waitForElementToBeClickable(WebDriver driver, By by){
        try{
            WebDriverWait wait = new WebDriverWait(driver, DEFAULT_SLEEP_TIMEOUT);
            WebElement el = wait.until(ExpectedConditions.elementToBeClickable(by));
            return el;
        }catch (TimeoutException ex){
            String msg = "error waiting for element by " + by + " url " + driver.getCurrentUrl() + " title " + driver.getTitle();
            throw new TimeoutException(msg, ex);
        }

    }

    public static WebElement waitForElementToBeVisible(WebDriver driver, By by){
        try{
            WebDriverWait wait = new WebDriverWait(driver, DEFAULT_SLEEP_TIMEOUT);
            WebElement el = wait.until(ExpectedConditions.visibilityOfElementLocated(by));
            return el;
        }catch (TimeoutException ex){
            String msg = "error waiting for element by " + by + " url " + driver.getCurrentUrl() + " title " + driver.getTitle();
            throw new TimeoutException(msg, ex);
        }

    }

    public static void sleepSec(long seconds){
        try{
            TimeUnit.SECONDS.sleep(seconds);
        }catch (InterruptedException ex){}
    }

}
