package sign.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by alexsergeev on 4/25/16.
 */
public class Locators {

    public static final By LOGIN_BTN = By.cssSelector("a.m-link-list--log-in");

    public static By pWithTextXpath(String text){
        return By.xpath("//p[text()[.='"+ text +"']]");
    }

    public interface LogIn {
        public static final By EMAIl_TXT = By.xpath("//div/input[@id='login_email_address_input']");
        public static final By PASSWORD_TXT = By.xpath("//div/input[@id='login_password_input']");
        public static final By SIGN_IN_BTN = By.xpath("//div/button/span[text()[.='Sign In']]");
        public static final By CLOSE_POPUP = By.xpath("//div[@id='login_modal']/a[contains(@class, 'close-reveal-modal')]");
    }

}
