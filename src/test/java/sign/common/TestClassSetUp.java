package sign.common;

import com.driver.Driver;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;

/**
 * Created by alexsergeev on 4/26/16.
 */
public class TestClassSetUp {

    protected WebDriver webDriver;

    @BeforeClass
    public void setUp(){
        webDriver = Driver.getWebDriver();
    }

}
