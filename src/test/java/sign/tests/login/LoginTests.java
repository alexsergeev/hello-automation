package sign.tests.login;

import org.testng.annotations.*;
import sign.common.Locators;
import sign.common.SeleniumUtils;
import sign.common.TestClassSetUp;

/**
 * Created by alexsergeev on 4/26/16.
 */
public class LoginTests extends TestClassSetUp{

    @BeforeMethod
    public void clickLoginBtn(){
        SeleniumUtils.assertElementExists(webDriver, Locators.LOGIN_BTN);
        SeleniumUtils.waitForElementToBeClickable(webDriver, Locators.LOGIN_BTN);
        SeleniumUtils.clickUsingJavascript(webDriver, Locators.LOGIN_BTN);
    }

    @AfterMethod
    public void closeLoginPopUp(){
        SeleniumUtils.waitForElementToBeVisible(webDriver, Locators.LogIn.CLOSE_POPUP);
        SeleniumUtils.click(webDriver, Locators.LogIn.CLOSE_POPUP);
        SeleniumUtils.sleepSec(2);
    }

    @Test
    public void testInvalidEmailAddress(){
        SeleniumUtils.waitForElementToBeVisible(webDriver, Locators.LogIn.EMAIl_TXT);
        SeleniumUtils.setText(webDriver, Locators.LogIn.EMAIl_TXT, "test");
        SeleniumUtils.setText(webDriver, Locators.LogIn.PASSWORD_TXT, "Asd123");
        SeleniumUtils.assertElementExists(webDriver, Locators.LogIn.SIGN_IN_BTN);
        SeleniumUtils.click(webDriver, Locators.LogIn.SIGN_IN_BTN);
        SeleniumUtils.waitForElementToBeVisible(webDriver, Locators.pWithTextXpath("Invalid email address"));
        SeleniumUtils.verifyThatTextIsPresent(webDriver, Locators.pWithTextXpath("Invalid email address"), "Invalid email address");
    }

    @Test
    public void testInvalidUserAndPassword(){
        SeleniumUtils.waitForElementToBeVisible(webDriver, Locators.LogIn.EMAIl_TXT);
        SeleniumUtils.setText(webDriver, Locators.LogIn.EMAIl_TXT, "test@example.com");
        SeleniumUtils.setText(webDriver, Locators.LogIn.PASSWORD_TXT, "Asd123");
        SeleniumUtils.assertElementExists(webDriver, Locators.LogIn.SIGN_IN_BTN);
        SeleniumUtils.click(webDriver, Locators.LogIn.SIGN_IN_BTN);
        SeleniumUtils.waitForElementToBeVisible(webDriver, Locators.pWithTextXpath("Invalid username/password combo."));
        SeleniumUtils.verifyThatTextIsPresent(webDriver, Locators.pWithTextXpath("Invalid username/password combo."), "Invalid username/password combo.");
    }

}
