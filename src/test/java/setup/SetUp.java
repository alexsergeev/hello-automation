package setup;

import com.driver.Driver;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

/**
 * Created by alexsergeev on 4/26/16.
 */
public class SetUp {

    private WebDriver webDriver = null;

    @BeforeSuite
    public void setUp(){
        webDriver = Driver.createWebDriver();
        webDriver.manage().window().maximize();
        webDriver.get("https://www.hellosign.com/");
//        webDriver.manage().timeouts().implicitlyWait(999, TimeUnit.MILLISECONDS);
        Assert.assertNotNull(Driver.getWebDriver());
    }

    @AfterSuite
    public void tearDown(){
        webDriver.quit();
    }

}
