package com.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by alexsergeev on 4/25/16.
 */
public class Driver {

    private static WebDriver webDriver = null;

    private Driver(){}

    public static WebDriver createWebDriver(){
        return createWebDriver("CHROME");
    }

    public static WebDriver createWebDriver(String browser){

        Browsers selectedBrowser = Browsers.CHROME;

        if(browser != null && !browser.isEmpty() && Browsers.valueOf(browser) != null){
            selectedBrowser = Browsers.valueOf(browser);
            System.out.println("Browser:" + selectedBrowser.toString());
        }

        switch (selectedBrowser){
            case CHROME:
                System.setProperty("webdriver.chrome.driver", "chromedriver");
                webDriver = new ChromeDriver();
                break;
            case FIREFOX:
                webDriver = new FirefoxDriver();
                break;
            default:
                System.setProperty("webdriver.chrome.driver", "chromedriver");
                webDriver = new ChromeDriver();
        }

        return webDriver;
    }

    public static WebDriver getWebDriver(){
        if(webDriver == null){
            return null;
        }
        return webDriver;
    }


    enum Browsers{ CHROME, FIREFOX }

}
